<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("p").click(function(){
                $(this).delay(50).fadeOut("slow");

            });
        });
    </script>
</head>
<body>

<p>If you click on me, I will disappear.</p>
<p>Click me away!</p>
<p>Click me too!</p>

</body>

<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#p1").delay(200).fadeOut("slow");
            $("#p2").delay(500).fadeOut("slow");
            $("#p3").delay(800).fadeOut("slow");
            $("#p1").delay(200).fadeIn("slow");
            $("#p2").delay(500).fadeIn("slow");
            $("#p3").delay(800).fadeIn("slow");
            $("p").click(function(){
                $(this).delay(50).fadeOut("slow");


            });
        });
    </script>
</head>
<body>

<p id="p1">If you click on me, I will disappear.</p>
<p id="p2">Click me away!</p>
<p id="p3">Click me too!</p>

</body>
