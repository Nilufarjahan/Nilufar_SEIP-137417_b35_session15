<?php

function factorial( $number ) {

        if ( $number == 1 ) {
        return 1;
        }

        $result = ( $number * factorial( $number-1 ) );
        return $result;
}

echo "The factorial of 5 is: " . factorial( 5 );

?>